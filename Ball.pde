/**
 * Ball class
 */
public class Ball{
  float x, y, r, m;
  float[] force = {0, 0};
  float[] speed = {0, 0};
  boolean if_tracking = false;
  int tp = 1200;
  float[][] tracks = new float[tp][2];
  int it = 0;
  /**
   * Class constructor 
   * 
   * @param x_pos   initial x positon
   * @param y_pos   initial y position
   * @param rad     radius of ball
   * @param affect  specify if gravity affects specific ball 
   * @return        null
   */
  Ball(float x_pos, float y_pos, float rad, boolean affect){
    x = x_pos;
    y = y_pos;
    r = rad;
    speed[0] = random(-0.1, 0.1);
    speed[1] = random(-0.1, 0.1);
    //calculate mass based on radius and global density
    if(affect)
      m = density * 3.1416 * r * r;
    else
      m = 0;
  }
  
  /**
   * Update position of ball
   *  
   * @return        null
   */
  public void update(){

    calculate_force();
    
    /*
    if(abs(Rx) > r){
      force_x = G*(this.m*mouseBall.m)/((Rx)*(Rx));
      if(Rx > 0)
        force_x = force_x * (-1);
    }
    if(abs(Ry) > r){
      force_y = G*(this.m*mouseBall.m)/((Ry)*(Ry));
      if(Ry > 0)
        force_y = force_y * (-1);
    }
    */
    speed[0] += force[0] * m;
    speed[1] += force[1] * m;
    x = x + speed[0];
    y = y + speed[1];
    //handle collision witch edge of window
    if(x < r/2 || x > sizeX - r/2){
      speed[0] = -B*speed[0];
      speed[1] = B*speed[1];
    }else if(y < r/2 || y > sizeY - r/2){
      speed[0] = B*speed[0];
      speed[1] = -B*speed[1];
    }
  }
  
  /**
   * Calculating forces acting on ball 
   *  
   * @return        null
   */
  private void calculate_force(){
   force[0] = 0;
   force[1] = 0;
   for(Ball target : Balls){ 
      //calculate distance
      float Rx = target.x-this.x;
      float Ry = target.y-this.y;
      float R = sqrt(Rx*Rx+Ry*Ry);
      //collision event
      if(R <= r/2 + target.r){
        
      }
      //calculate force
      else if(R > r + target.r){
        float F = G*(target.m*this.m)/(R*R);
        force[0] += (F*Rx)/R;
        force[1] += (F*Ry)/R;
      }
   }
  }
  /**
   * Printing ball on window 
   *  
   * @return        null
   */
  public void pop(){
    if(if_tracking){
      if(it >= tp)
        it = 0;
      tracks[it][0] = x;
      tracks[it][1] = y;
      it++;
      for(int i = 0; i < tp; i++){
        //noSmooth();
        stroke(5);
        fill(0);
        point(tracks[i][0], tracks[i][1]);
      }
      fill(255);
    }else
      fill(0);
    noStroke();
    circle(x, y, r);
  }
  /**
   * Update ball base on mouse position
   *  
   * @return        null
   */
  public void updateMouse(){
    x = mouseX;
    y = mouseY;
    noStroke();
    fill(0);
    circle(x, y, r);
    //force_x = G*(this.m*Balls[5].m)/((this.x-Balls[5].x)*(this.x-Balls[5].x));
   // println(force_x);
  }
  
  void tracking(boolean t){
    if_tracking = t;
  }
  
  
}
