/**
 * Simple visualisation of balls 
 * 
 * https://gitlab.com/MichoP/gravityconnect
 * 
 * @author Michał Adamus
 * @version 0.1
 */

Ball mouseBall;
int segments = 50;                  //nuber of segments
Ball[] Balls = new Ball[segments];
int sizeX = 1000;                   //widow size
int sizeY = 500;
float segmentX, segmentY;
int light = 180;                    //background color 
//simulation parameters 
float density = 1;
float G = 0.00000001;
float B = 0.95;
boolean debug = false;

void setup(){
  size(1000, 500);                  //widow size match with sizeX and sizeY
  background(200);
  frameRate(60);
  //creating segments
  int x_seg, y_seg;
  
  x_seg = 1;
  y_seg = segments/x_seg;
  
  boolean serch = true;
  while(serch){
    if(segments % x_seg == 0){
      y_seg = segments/x_seg;
    }
    if(x_seg >= y_seg){
      serch = false;
      x_seg--;
    }
    x_seg++;
  }
  if(debug){
    String output = "";
    output = segments + "=" + y_seg + ":" + x_seg;
    println(output);
  }
  
  segmentX = sizeX/x_seg;
  segmentY = sizeY/y_seg;
  mouseBall = new Ball(mouseX, mouseY, 20, true);
  
  //creating ball in random position for each segment
  int i = 0;
  for(int x = 0; x < x_seg; x++){
    float currX = x*segmentX;
    for(int y = 0; y < y_seg; y++){
      float currY = y*segmentY;
      float r = random(5,15);
      Balls[i] = new Ball(currX + random(0.1, 0.9)*segmentX, currY + random(0.1, 0.9)*segmentY, r, true);
      i++;
      //circle(x + random(1)*segmentX, y + random(1)*segmentY, 20);
    }
  }
  //Log x position foe each ball
  if(debug){
    for( Ball x : Balls)
    {
      println(x.x);
    }
  }
}

void draw(){
  background(light);
  //update each ball
  for(Ball i : Balls){
    i.update();
    connect(mouseBall, i);
    i.pop();
  }
  //set ball 0 tracking
  Balls[0].tracking(true);
  if(debug)
    drawGrid();
  mouseBall.updateMouse();
}

void connect(Ball f, Ball s){
  int radius = 320;
  float d = distance(f.x, f.y, s.x, s.y);
  if (d < radius){
    float l = map(d, radius, 0, light, 255-light);
    stroke(l);
    line(f.x, f.y, s.x, s.y);
  }
}

float distance(float x1, float y1, float x2, float y2){
  float d = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
  return d;
}

void drawGrid(){
  stroke(0);
  for(int x = 0; x < sizeX; x += segmentX){
    line(x, 0, x, 500);
    for(int y = 0; y <sizeY; y += segmentY){
      line(0, y, 1000, y);
    }
  }
}
